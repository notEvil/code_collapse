import libcst


# order based on https://docs.python.org/3/library/ast.html
_v_ = ["params", "posonly_params", "star_arg", "kwonly_params", "star_kwarg"]
_ATTRIBUTE_NAMES = {
    libcst.Module: ["body"],
    # statement
    libcst.FunctionDef: ["decorators", "params"],
    libcst.ClassDef: ["decorators", "bases", "keywords"],
    libcst.Return: ["value"],
    libcst.Del: ["target"],
    libcst.Assign: ["value"],
    libcst.AugAssign: ["value"],
    libcst.AnnAssign: ["value"],
    libcst.For: ["iter"],
    libcst.While: [],
    libcst.If: ["test"],
    libcst.With: ["items"],
    libcst.WithItem: ["item"],
    libcst.Match: ["subject"],
    libcst.Raise: ["exc", "cause"],
    libcst.Try: ["handlers"],
    libcst.ExceptHandler: ["type"],
    libcst.Assert: ["test"],
    libcst.Import: [],
    libcst.ImportFrom: [],
    libcst.Global: [],
    libcst.Nonlocal: [],
    libcst.Expr: ["value"],
    libcst.Pass: [],
    libcst.Break: [],
    libcst.Continue: [],
    # expr
    libcst.BooleanOperation: ["left"],
    libcst.BinaryOperation: ["left", "right"],
    libcst.UnaryOperation: ["expression"],
    libcst.Lambda: ["params"],
    libcst.IfExp: ["test"],
    libcst.Dict: ["elements"],
    libcst.DictElement: ["key", "value"],
    libcst.StarredDictElement: ["value"],
    libcst.Set: ["elements"],
    libcst.ListComp: ["for_in"],
    libcst.SetComp: ["for_in"],
    libcst.DictComp: ["for_in"],
    libcst.GeneratorExp: ["for_in"],
    libcst.CompFor: ["iter"],
    libcst.Comparison: ["left", "comparisons"],
    libcst.ComparisonTarget: ["comparator"],
    libcst.Call: ["func", "args"],
    libcst.Await: ["expression"],
    libcst.Yield: ["value"],
    libcst.Attribute: ["value"],
    libcst.Subscript: ["value", "slice"],
    libcst.SubscriptElement: ["slice"],
    libcst.Index: ["value"],
    libcst.Name: [],
    libcst.List: ["elements"],
    libcst.Tuple: ["elements"],
    libcst.Slice: ["lower", "upper", "step"],
    # other
    libcst.Integer: [],
    libcst.Float: [],
    libcst.Imaginary: [],
    libcst.SimpleString: [],
    libcst.ConcatenatedString: ["left", "right"],
    libcst.FormattedString: ["parts"],
    libcst.FormattedStringText: [],
    libcst.FormattedStringExpression: ["expression"],
    libcst.Element: ["value"],
    libcst.StarredElement: ["value"],
    libcst.Parameters: _v_,
    libcst.Param: ["default"],
    libcst.ParamSlash: [],
    libcst.ParamStar: [],
    libcst.Arg: ["value"],
    libcst.Decorator: ["decorator"],
    libcst.From: ["item"],
    #
    libcst.SimpleStatementLine: ["body"],
    libcst.Ellipsis: [],
}


def get_sub_nodes(cst_node):
    for attribute_name in _ATTRIBUTE_NAMES[type(cst_node)]:
        attribute_value = getattr(cst_node, attribute_name)
        if attribute_value is None:
            continue

        if isinstance(attribute_value, libcst.MaybeSentinel):
            continue

        if not isinstance(attribute_value, libcst.CSTNode):
            yield from attribute_value
            continue

        yield attribute_value
