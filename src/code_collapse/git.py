import pathlib
import re
import subprocess
import typing


def get_repository_path(path):
    path = path.resolve()
    while not (path / ".git").exists():
        if path.parent == path:
            return
        path = path.parent
    return path


def get_diff_lines(repository_path, path=None, head=True, stderr=None):
    if path is None:
        path = repository_path

    _v_ = ["git", "diff", "--unified=0", "HEAD" if head else None, "--", str(path)]
    _v_ = list(filter(None, _v_))
    process = subprocess.Popen(
        _v_, stdout=subprocess.PIPE, stderr=stderr, cwd=repository_path
    )

    for line_bytes in typing.cast(typing.IO, process.stdout):
        yield line_bytes.decode()

    return_code = process.wait()
    if return_code != 0:
        raise ReturnCodeError(return_code)


class ReturnCodeError(Exception):
    def __init__(self, return_code):
        super().__init__()

        self.return_code = return_code


def get_line_ranges(diff_lines, repository_path):
    line_ranges = []
    path = None

    for line_string in diff_lines:
        if line_string.startswith("+++ ") or line_string.startswith("--- "):
            if len(line_ranges) != 0:
                yield (path, line_ranges)
                line_ranges = []

            _v_ = pathlib.Path(line_string[4:-1]).parts[1:]
            path = pathlib.Path(repository_path, *_v_)
            continue

        if line_string.startswith("@@ "):
            _v_ = re.search(
                r"^@@ -(?P<l1>\d+)(,(?P<s1>\d+))? \+(?P<l2>\d+)(,(?P<s2>\d+))? @@",
                line_string,
            )
            match = typing.cast(re.Match, _v_)

            l1, s1, l2, s2 = match.group("l1", "s1", "l2", "s2")
            l1 = int(l1)
            s1 = 1 if s1 is None else int(s1)
            l2 = int(l2)
            s2 = 1 if s2 is None else int(s2)

            line_ranges.append(((l1, l1 + s1), (l2, l2 + s2)))

    if len(line_ranges) != 0:
        yield (path, line_ranges)
