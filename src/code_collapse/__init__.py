import code_collapse._scope as c__scope
import black
import black.mode as b_mode
import black.parsing as b_parsing
import libcst
import libcst.matchers as l_matchers
import libcst.metadata as l_metadata
import collections
import concurrent.futures as c_futures
import contextlib
import dataclasses
import datetime
import difflib
import pathlib
import re
import sys
import typing


def _get_assign(target_node, value_node):
    _v_ = [libcst.AssignTarget(target=target_node)]
    _v_ = [libcst.Assign(targets=_v_, value=_wrap_node(value_node))]
    return libcst.SimpleStatementLine(body=_v_)


def _wrap_node(cst_node):
    left_parens = [] if cst_node.lpar is None else list(cst_node.lpar)
    right_parens = [] if cst_node.rpar is None else list(cst_node.rpar)
    left_parens.insert(0, libcst.LeftParen())
    right_parens.append(libcst.RightParen())
    return cst_node.with_changes(lpar=left_parens, rpar=right_parens)


def _get_string(cst_nodes):
    return libcst.Module(body=cst_nodes).code


@dataclasses.dataclass
class Configuration:
    _v_ = lambda: b_mode.Mode(magic_trailing_comma=False)
    black_mode: b_mode.Mode = dataclasses.field(default_factory=_v_)

    collapse_name: bool = False
    collapse_self: bool = False
    collapse_call_name: bool = False
    collapse_default: bool = True
    collapse_keyword_argument: bool = True
    collapse_assert: bool = False
    collapse_statement: bool = True
    collapse_literal: bool = False
    temporary_name: str = "_v_"
    _prefer_bottom: bool = False


class FStringError(Exception):
    pass


def collapse_code(
    code_string: str,
    expand=False,
    configuration: Configuration | None = None,
    executor: c_futures.Executor | None = None,
    _return_nodes: bool = False,
) -> str | list[libcst.CSTNode]:
    if configuration is None:
        configuration = Configuration()

    indentation = _get_indentation(code_string)

    module_code = (" " * (indentation * 4)) + code_string.strip()

    # insert `def _():`
    strings = []
    for index in range(indentation):
        strings.append(" " * (index * 4))
        strings.append("def _():\n")
    strings.append(module_code)
    module_code = "".join(strings)
    #

    cst_node, has_pass = _parse_module(module_code)
    metadata_wrapper = l_metadata.MetadataWrapper(cst_node)

    # get node
    cst_node = metadata_wrapper.module
    for _ in range(indentation):
        cst_node = typing.cast(libcst.FunctionDef, cst_node.body[0]).body
    #

    _v_ = _get_nodes(cst_node, configuration.temporary_name)
    result_nodes, temporary_nodes, last_node = _v_

    # expand last node
    _v_ = _get_access_nodes(metadata_wrapper, indentation, configuration.temporary_name)
    access_nodes = _v_

    temporary_node = None

    try:
        for statement_node in temporary_nodes:
            _v_ = _SubstituteTransformer(access_nodes, temporary_node)
            statement_node = statement_node.visit(_v_)

            match statement_node:
                case libcst.SimpleStatementLine(
                    body=[libcst.Assign(value=libcst.Yield())]
                ) | libcst.SimpleStatementLine(
                    trailing_whitespace=libcst.TrailingWhitespace(
                        comment=libcst.Comment()
                    )
                ):
                    checkpoint = True

                case _:
                    whitespace_transformer = _WhitespaceTransformer()

                    _v_ = statement_node.body[0].value.visit(whitespace_transformer)
                    temporary_node = _v_

                    checkpoint = whitespace_transformer._has_any_comment

            if checkpoint:
                if expand:
                    result_nodes.append(statement_node)

                else:
                    _v_ = configuration.black_mode
                    _v_ = collapse_code(
                        "\n".join(_format_nodes([statement_node], indentation, _v_)),
                        expand=False,
                        configuration=configuration,
                        executor=executor,
                        _return_nodes=True,
                    )
                    result_nodes.extend(_v_)

                temporary_node = libcst.Name(value=configuration.temporary_name)

        _v_ = last_node.visit(_SubstituteTransformer(access_nodes, temporary_node))
        last_node = _v_

    except _SubstituteException:
        return code_string
    #

    match last_node:
        case libcst.SimpleStatementLine(
            trailing_whitespace=libcst.TrailingWhitespace(
                comment=libcst.Comment(value="# col: skip")
            )
        ):
            expand = True

        case object(
            body=libcst.IndentedBlock(
                header=libcst.TrailingWhitespace(
                    comment=libcst.Comment(value="# col: skip")
                )
            )
        ) if isinstance(last_node, (libcst.FunctionDef, libcst.ClassDef)):
            expand = True

    if expand:
        min_nodes = [last_node]

    else:
        _v_ = _Candidates(indentation, configuration).get(last_node, (0, 0), executor)
        tuples = iter(_v_)

        cst_nodes, min_size = _except(next, last_node)(tuples)
        min_nodes = list(cst_nodes)

        for cst_nodes, size in tuples:
            if size < min_size:
                min_nodes = list(cst_nodes)
                if configuration._prefer_bottom:
                    min_nodes.reverse()

                min_size = size

    result_nodes.extend(min_nodes)

    line_strings = _except(_format_nodes, last_node)(
        result_nodes, indentation, configuration.black_mode, has_pass=has_pass
    )
    return result_nodes if _return_nodes else "\n".join(line_strings)


def _get_indentation(code_string):
    match = typing.cast(re.Match, re.search(r"^[ ]*\S", code_string, re.MULTILINE))
    return (match.end() - match.start() - 1) // 4


def _parse_module(module_code):
    try:
        return (libcst.parse_module(module_code), False)

    except libcst.ParserSyntaxError as syntax_error:
        (message_string,) = syntax_error.args

        if not message_string.endswith("expected INDENT"):
            raise syntax_error

        # add `pass`
        _v_ = re.search(r"(^|\n)(?P<indent>[ ]*)\S[^\n]*$", module_code)
        match = typing.cast(re.Match, _v_)

        _v_ = " " * (((match.end("indent") - match.start("indent")) // 4 + 1) * 4)
        return (libcst.parse_module(f"{module_code}\n{_v_}pass"), True)
        #


def _get_nodes(cst_node, temporary_name):
    node_iterator = iter(reversed(cst_node.body))

    last_node = next(node_iterator)

    temporary_nodes = []

    for cst_node in node_iterator:
        match cst_node:
            case libcst.SimpleStatementLine(
                body=[
                    libcst.Assign(
                        targets=[libcst.AssignTarget(target=libcst.Name(value=name))]
                    )
                ]
            ) if name == temporary_name:
                temporary_nodes.append(cst_node)

            case _:
                break

    else:
        cst_node = None

    top_nodes = list(node_iterator)
    top_nodes.reverse()
    if cst_node is not None:
        top_nodes.append(cst_node)

    temporary_nodes.reverse()

    return (top_nodes, temporary_nodes, last_node)


def _get_access_nodes(metadata_wrapper, indentation, temporary_name):
    for cst_scope in set(metadata_wrapper.resolve(l_metadata.ScopeProvider).values()):
        current_scope = cst_scope

        for _ in range(indentation):
            if isinstance(current_scope, l_metadata.GlobalScope):
                break

            current_scope = current_scope.parent

        else:
            if isinstance(current_scope, l_metadata.GlobalScope):
                break
    else:
        raise Exception  # shouldn't happen

    return [access.node for access in cst_scope.accesses[temporary_name]]


class _WhitespaceTransformer(libcst.CSTTransformer):
    def __init__(self):
        super().__init__()

        self._has_comment = None
        self._has_any_comment = False

    def visit_ParenthesizedWhitespace(self, node):
        self._has_comment = False

    def visit_Comment(self, node):
        self._has_comment = True
        self._has_any_comment = True

    def leave_ParenthesizedWhitespace(self, original_node, updated_node):
        if self._has_comment:
            return original_node

        return libcst.SimpleWhitespace(value=" ")


class _Candidates:
    def __init__(self, indentation, configuration):
        super().__init__()

        self.indentation = indentation
        self.configuration = configuration

        self._cst_nodes = []
        self._sub_nodes = _SubNodes(configuration)
        self._temporary_node = libcst.Name(value=configuration.temporary_name)

    def get(self, statement_node, current_size, executor):
        # statement
        self._cst_nodes.append(statement_node)

        _v_ = self.configuration.black_mode
        _v_ = _get_size(self._cst_nodes[-1:], self.indentation, _v_)
        yield (self._cst_nodes, _add_sizes(current_size, _v_))

        self._cst_nodes.pop()

        # get parent and leaf nodes
        node_queue = collections.deque([statement_node])
        parent_nodes = {}
        leaf_nodes = []

        while node_queue:
            cst_node = node_queue.popleft()  # breadth first
            any = False

            for sub_node in self._sub_nodes.get(cst_node, None):
                node_queue.append(sub_node)
                parent_nodes[sub_node] = cst_node
                any = True

            if not any:
                leaf_nodes.append(cst_node)
        #

        tuples = _prepare_candidates(
            parent_nodes,
            leaf_nodes,
            statement_node,
            self._temporary_node,
            self.indentation,
            self.configuration,
            executor,
        )

        if executor is None:
            sub_candidates = _SubCandidates(self)

            for cst_node, collapsed_node, collapsed_size in tuples:
                self._cst_nodes.append(collapsed_node)

                size = _add_sizes(current_size, collapsed_size)

                if self.configuration._prefer_bottom:
                    _v_ = _get_assign(self._temporary_node, cst_node)
                    yield from self.get(_v_, size, None)

                else:
                    _v_ = statement_node.deep_replace(cst_node, self._temporary_node)
                    yield from sub_candidates.get(_v_, size)

                self._cst_nodes.pop()

        else:
            for future in [
                executor.submit(
                    _get_sub_candidates,
                    tuple,
                    statement_node,
                    self.indentation,
                    self.configuration,
                )
                for tuple in tuples
            ]:
                yield from future.result()


def _get_sub_candidates(tuple, statement_node, indentation, configuration):
    candidates = _Candidates(indentation, configuration)

    cst_node, collapsed_node, collapsed_size = tuple
    candidates._cst_nodes.append(collapsed_node)

    if configuration._prefer_bottom:
        _v_ = _get_assign(candidates._temporary_node, cst_node)
        tuples = candidates.get(_v_, collapsed_size, None)

    else:
        _v_ = statement_node.deep_replace(cst_node, candidates._temporary_node)
        tuples = _SubCandidates(candidates).get(_v_, collapsed_size)

    return [(list(cst_nodes), size) for cst_nodes, size in tuples]


class _SubCandidates:
    def __init__(self, candidates):
        super().__init__()

        self.candidates = candidates

    def get(self, statement_node, current_size):
        # statement
        self.candidates._cst_nodes.append(statement_node)

        _v_ = self.candidates.configuration.black_mode
        _v_ = _get_size([statement_node], self.candidates.indentation, _v_)
        yield (self.candidates._cst_nodes, _add_sizes(current_size, _v_))

        self.candidates._cst_nodes.pop()

        # get parent nodes
        path_visitor = _PathVisitor(self.candidates._temporary_node)
        statement_node.visit(path_visitor)

        parent_node = statement_node
        sub_nodes = list(self.candidates._sub_nodes.get(parent_node, None))
        parent_nodes = {}

        for path_node in path_visitor._cst_nodes[1:]:
            if path_node not in sub_nodes:
                continue

            parent_nodes[path_node] = parent_node

            parent_node = path_node
            sub_nodes = list(self.candidates._sub_nodes.get(parent_node, None))
        #

        for cst_node, collapsed_node, collapsed_size in _prepare_candidates(
            parent_nodes,
            [parent_node],
            statement_node,
            self.candidates._temporary_node,
            self.candidates.indentation,
            self.candidates.configuration,
            None,
        ):
            self.candidates._cst_nodes.append(collapsed_node)

            _v_ = statement_node.deep_replace(cst_node, self.candidates._temporary_node)
            yield from self.get(_v_, _add_sizes(current_size, collapsed_size))

            self.candidates._cst_nodes.pop()


class _PathVisitor(libcst.CSTVisitor):
    def __init__(self, temporary_node):
        super().__init__()

        self.temporary_node = temporary_node

        self._found = False
        self._cst_nodes = []

    def on_visit(self, cst_node):
        if self._found:
            return False

        if cst_node is self.temporary_node:
            self._found = True
            return False

        self._cst_nodes.append(cst_node)
        return True

    def on_leave(self, original_node):
        if self._found:
            return

        self._cst_nodes.pop()


def _prepare_candidates(
    parent_nodes,
    leaf_nodes,
    statement_node,
    temporary_node,
    indentation,
    configuration,
    executor,
):
    if executor is None:
        candidates = {}

        for cst_node in parent_nodes:
            collapsed_node = (
                statement_node.deep_replace(cst_node, temporary_node)
                if configuration._prefer_bottom
                else _get_assign(temporary_node, cst_node)
            )

            _v_ = _get_size([collapsed_node], indentation, configuration.black_mode)
            candidates[cst_node] = (collapsed_node, _v_)

    else:
        _statement_node = statement_node if configuration._prefer_bottom else None

        _v_ = [
            executor.submit(
                _get_candidate,
                cst_node,
                _statement_node,
                temporary_node,
                indentation,
                configuration.black_mode,
                configuration._prefer_bottom,
            )
            for cst_node in parent_nodes
        ]
        _v_ = {cst_node: future.result() for cst_node, future in zip(parent_nodes, _v_)}
        candidates = _v_

    statement_size = (
        _get_size([statement_node], indentation, configuration.black_mode)
        if configuration._prefer_bottom
        else None
    )

    tuples = []
    for cst_node, parent_node in parent_nodes.items():
        collapsed_node, collapsed_size = candidates[cst_node]
        parent_tuple = candidates.get(parent_node)

        _v_ = configuration._prefer_bottom and cst_node in leaf_nodes
        if _v_ and collapsed_size[0] != typing.cast(tuple, statement_size)[0]:
            tuples.append((cst_node, collapsed_node, collapsed_size))

        if parent_tuple is None:
            if configuration._prefer_bottom:
                continue

            tuples.append((cst_node, collapsed_node, collapsed_size))
            continue

        parent_collapsed_node, parent_collapsed_size = parent_tuple

        if collapsed_size[0] != parent_collapsed_size[0]:
            tuples.append(
                (parent_node, parent_collapsed_node, parent_collapsed_size)
                if configuration._prefer_bottom
                else (cst_node, collapsed_node, collapsed_size)
            )

    def _key(tuple):
        line, characters = tuple[2]
        return (line, -characters)

    tuples.sort(key=_key)
    return tuples


def _get_candidate(
    cst_node, statement_node, temporary_node, indentation, black_mode, prefer_bottom
):
    collapsed_node = (
        statement_node.deep_replace(cst_node, temporary_node)
        if prefer_bottom
        else _get_assign(temporary_node, cst_node)
    )
    return (collapsed_node, _get_size([collapsed_node], indentation, black_mode))


class _SubNodes:
    _UNPACK_TYPES = [
        libcst.Arg,
        libcst.ComparisonTarget,
        libcst.CompFor,
        libcst.Decorator,
        libcst.DictElement,
        libcst.Element,
        libcst.Ellipsis,
        libcst.ExceptHandler,
        libcst.FormattedStringExpression,
        libcst.FormattedStringText,
        libcst.From,
        libcst.Index,
        libcst.Param,
        libcst.Parameters,
        libcst.ParamSlash,
        libcst.ParamStar,
        libcst.Slice,
        libcst.StarredDictElement,
        libcst.StarredElement,
        libcst.SubscriptElement,
        libcst.WithItem,
    ]

    def __init__(self, configuration):
        super().__init__()

        self.configuration = configuration

        self._unpack_types = list(type(self)._UNPACK_TYPES)

        if not configuration.collapse_name:
            self._unpack_types.append(libcst.Name)

        if not configuration.collapse_literal:
            self._unpack_types.extend([libcst.Integer, libcst.Float, libcst.Imaginary])

    def get(self, cst_node, white_node):
        ignore_nodes = []
        unpack_nodes = []

        match cst_node:
            case libcst.Call(
                func=libcst.Name() | libcst.Attribute()
            ) if not self.configuration.collapse_call_name:
                unpack_nodes.append(cst_node.func)

            case libcst.Param(
                default=default_node
            ) if not self.configuration.collapse_default and default_node is not None:
                unpack_nodes.append(default_node)

            case libcst.Arg(
                keyword=keyword_node
            ) if not self.configuration.collapse_keyword_argument and keyword_node is not None:
                unpack_nodes.append(cst_node.value)

            case libcst.ConcatenatedString():
                unpack_nodes.extend([cst_node.left, cst_node.right])

            case libcst.Del(target=target_node):
                if not isinstance(target_node, (libcst.Attribute, libcst.Subscript)):
                    return

                unpack_nodes.append(target_node)

            case libcst.Assert() if not self.configuration.collapse_assert:
                return

            case (
                libcst.Global()
                | libcst.Import()
                | libcst.ImportFrom()
                | libcst.Nonlocal()
            ):
                return

            case libcst.Assign(
                targets=[libcst.AssignTarget(target=libcst.Name(value=name))]
            ) if name == self.configuration.temporary_name:
                unpack_nodes.append(cst_node.value)

            case libcst.BinaryOperation(
                left=libcst.BaseNumber(),
                operator=libcst.Add(),
                right=libcst.Imaginary(),
            ):
                return

            case object(
                value=libcst.Call(
                    func=libcst.Attribute(
                        value=libcst.Name(value="typing"),
                        attr=libcst.Name(value="TypeVar"),
                    )
                    | libcst.Name(value="TypeVar")
                )
            ) | libcst.AnnAssign(
                annotation=libcst.Annotation(
                    annotation=libcst.Attribute(
                        value=libcst.Name(value="typing"), attr=libcst.Name("TypeAlias")
                    )
                    | libcst.Name("TypeAlias")
                )
            ) if isinstance(
                cst_node, (libcst.Assign, libcst.AnnAssign)
            ):
                return

        match cst_node:
            case libcst.With():
                ignore_nodes.extend(cst_node.items[1:])

            case libcst.Call(
                func=libcst.Attribute(
                    value=libcst.Name(value="typing"), attr=libcst.Name(value="cast")
                )
            ):
                ignore_nodes.append(cst_node.args[0])

        for cst_node in c__scope.get_sub_nodes(cst_node):
            if cst_node is white_node:
                yield white_node
                continue

            if isinstance(cst_node, libcst.BaseSmallStatement):
                if self.configuration.collapse_statement:
                    yield from self.get(cst_node, white_node)

                else:
                    for cst_node in c__scope.get_sub_nodes(cst_node):
                        yield from self.get(cst_node, white_node)

                continue

            if cst_node in ignore_nodes:
                continue

            if type(cst_node) in self._unpack_types or cst_node in unpack_nodes:
                yield from self.get(cst_node, white_node)
                continue

            if not self.configuration.collapse_self:
                match cst_node:
                    case libcst.Attribute(value=libcst.Name(value="self")):
                        continue

            if not self.configuration.collapse_literal:
                match cst_node:
                    case libcst.SimpleString(
                        quote=quote_string
                    ) if quote_string not in ("'''", '"""'):
                        continue

                    case libcst.BinaryOperation(
                        left=libcst.BaseNumber(),
                        operator=libcst.Add(),
                        right=libcst.Imaginary(),
                    ):
                        continue

            yield cst_node


class _SubstituteTransformer(libcst.CSTTransformer):
    _OPERATOR_PRECEDENCE = {}

    index = 0

    _v_ = libcst.MatrixMultiply
    for index, types in enumerate([
        [libcst.NamedExpr, libcst.Yield],
        [libcst.Lambda],
        [libcst.IfExp],
        [libcst.Or],
        [libcst.And],
        [libcst.Not],
        [libcst.Comparison, libcst.ComparisonTarget],
        [libcst.BitOr],
        [libcst.BitXor],
        [libcst.BitAnd],
        [libcst.LeftShift, libcst.RightShift],
        [libcst.Add, libcst.Subtract],
        [libcst.Multiply, _v_, libcst.Divide, libcst.FloorDivide, libcst.Modulo],
        [libcst.Plus, libcst.Minus, libcst.BitInvert, libcst.Power],
        [libcst.Await],
        [libcst.Attribute, libcst.Subscript, libcst.Call],
    ]):
        for type_ in types:
            _OPERATOR_PRECEDENCE[type_] = index

    _LEAST_PRECEDENCE = index + 1

    _PARENTHESIZE_ARGUMENTS = {
        libcst.ComparisonTarget: [("comparator", False)],
        libcst.Comparison: [("left", False)],
        libcst.UnaryOperation: [("expression", False)],
        libcst.BinaryOperation: [("left", False), ("right", True)],
        libcst.BooleanOperation: [("left", False), ("right", True)],
        libcst.Attribute: [("value", False)],
        libcst.Subscript: [("value", False)],
        libcst.Lambda: [("body", False)],
        libcst.Call: [("func", False)],
        libcst.Await: [("expression", False)],
        libcst.IfExp: [("body", False), ("test", True), ("orelse", True)],
        libcst.NamedExpr: [("target", False), ("value", True)],
        libcst.CompFor: [("iter", False)],
    }

    _v_ = [libcst.UnaryOperation, libcst.BinaryOperation, libcst.BooleanOperation]
    _OPERATOR_TYPES = _v_

    def __init__(self, cst_nodes, substitute_node):
        super().__init__()

        self.cst_nodes = cst_nodes
        self.substitute_node = substitute_node

        self._stack = [None]

    def on_visit(self, cst_node):
        self._stack.append(cst_node)

        return cst_node not in self.cst_nodes

    def on_leave(self, original_node, updated_node):
        self._stack.pop()

        if original_node in self.cst_nodes:
            if self.substitute_node is None:
                raise _SubstituteException

            parent_node = self._stack[-1]

            # based on https://github.com/Instagram/LibCST/pull/458
            _v_ = isinstance(parent_node, libcst.BinaryOperation)
            if _v_ and isinstance(parent_node.operator, libcst.Power):
                _v_ = original_node is parent_node.left
                return self._parenthesize(self.substitute_node, wrap_ties=_v_)

            _v_ = type(self)._PARENTHESIZE_ARGUMENTS.get(type(parent_node))
            parenthesize_arguments = _v_

            if parenthesize_arguments is not None:
                for attribute_name, wrap_ties in parenthesize_arguments:
                    if getattr(parent_node, attribute_name) is original_node:
                        return self._parenthesize(
                            self.substitute_node, wrap_ties=wrap_ties
                        )
            #

            return self.substitute_node

        return updated_node

    def _parenthesize(self, cst_node, wrap_ties=False):
        if self._is_parenthesized(cst_node):
            return cst_node

        parent_node = self._stack[-1]

        node_precedence = self._get_precedence(cst_node)
        parent_precedence = self._get_precedence(parent_node)

        if (
            (node_precedence <= parent_precedence)
            if wrap_ties
            else (node_precedence < parent_precedence)
        ):
            return _wrap_node(cst_node)

        return cst_node

    def _is_parenthesized(self, cst_node):
        return len(cst_node.lpar) != 0 and len(cst_node.rpar) != 0

    def _get_precedence(self, cst_node):
        if type(cst_node) in type(self)._OPERATOR_TYPES:
            cst_node = cst_node.operator

        _v_ = type(self)._OPERATOR_PRECEDENCE
        return _v_.get(type(cst_node), type(self)._LEAST_PRECEDENCE)


class _SubstituteException(Exception):
    pass


def _get_size(cst_nodes, indentation, black_mode):
    line_strings = _format_nodes(cst_nodes, indentation, black_mode)
    return (len(line_strings), sum(map(len, line_strings)))


def _add_sizes(first_size, second_size):
    first_lines, first_characters = first_size
    second_lines, second_characters = second_size
    return (first_lines + second_lines, first_characters + second_characters)


def _format_nodes(cst_nodes, indentation, black_mode, has_pass=False):
    for _ in range(indentation):
        _v_ = libcst.IndentedBlock(body=cst_nodes)
        _v_ = libcst.FunctionDef(
            name=libcst.Name(value="_"), params=libcst.Parameters(), body=_v_
        )
        cst_nodes = [_v_]

    _v_ = black.format_str(_get_string(cst_nodes), mode=black_mode).splitlines()
    return _v_[indentation : (-1 if has_pass else None)]


def _except(function, last_node):
    def _except(*args, **kwargs):
        try:
            return function(*args, **kwargs)

        except b_parsing.InvalidInput as exception:
            formatted_string = l_matchers.FormattedString()
            any_string = formatted_string | l_matchers.SimpleString()

            for f_string_node in l_matchers.findall(last_node, formatted_string):
                f_string_node = typing.cast(libcst.FormattedString, f_string_node)
                for string_node in l_matchers.findall(f_string_node, any_string):
                    string_node = typing.cast(
                        libcst.FormattedString | libcst.SimpleString, string_node
                    )

                    _v_ = "{0}{1}{1}{0}".format(f_string_node.quote, string_node.quote)
                    string = _v_

                    try:
                        libcst.parse_expression(string)

                    except Exception:
                        raise FStringError

            raise exception

    return _except


def collapse_file(
    path: pathlib.Path,
    expand: bool = False,
    configuration: Configuration | None = None,
    line_ranges: set[tuple[int, int]] | None = None,
    on_f_string_error: typing.Callable[[Exception, int, str], None] | None = None,
    progress: bool = False,
    datetime_: datetime.datetime | None = None,
    executor: c_futures.Executor | None = None,
    statements: bool = False,
):
    if datetime_ is None:
        datetime_ = datetime.datetime.now()

    if configuration is None:
        configuration = Configuration()

    with open(path) as file:
        code_string = file.read()

    line_strings = code_string.splitlines()
    collapsed_lines = list(line_strings)
    offset = 0
    any = False

    for start_line, end_line, code_string, collapsed_string in _collapse_file(
        code_string,
        line_strings,
        expand,
        configuration,
        line_ranges,
        on_f_string_error,
        executor,
        statements,
    ):
        if collapsed_string != code_string:
            length = len(collapsed_lines)

            _v_ = collapsed_string.splitlines()
            collapsed_lines[start_line - 1 + offset : end_line + offset] = _v_

            offset += len(collapsed_lines) - length

            any = True

        if progress:
            sys.stderr.write(".")
            sys.stderr.flush()

    if not any:
        return

    _v_ = datetime.datetime.fromtimestamp(path.stat().st_mtime_ns * 1e-9).isoformat()
    for line_string in difflib.unified_diff(
        line_strings,
        collapsed_lines,
        fromfile=str("a" / path),
        tofile=str("b" / path),
        fromfiledate=_v_,
        tofiledate=datetime_.isoformat(),
        lineterm="",
    ):
        yield line_string


def _collapse_file(
    code_string,
    line_strings,
    expand,
    configuration,
    line_ranges,
    on_f_string_error,
    executor,
    statements,
):
    file_visitor = _FileVisitor(line_strings, line_ranges, configuration.temporary_name)
    l_metadata.MetadataWrapper(libcst.parse_module(code_string)).visit(file_visitor)

    line_ranges = sorted(file_visitor._line_ranges)

    if executor is None or not statements:
        for start_line, end_line in line_ranges:
            code_string = "\n".join(line_strings[start_line - 1 : end_line])

            try:
                _v_ = collapse_code(
                    code_string,
                    expand=expand,
                    configuration=configuration,
                    executor=executor,
                )
                yield (start_line, end_line, code_string, typing.cast(str, _v_))

            except FStringError as exception:
                if on_f_string_error is None:
                    raise exception

                on_f_string_error(exception, start_line, code_string)
                continue

    else:
        code_strings = [
            "\n".join(line_strings[start_line - 1 : end_line])
            for start_line, end_line in line_ranges
        ]

        _v_ = [
            executor.submit(
                collapse_code, code_string, configuration=configuration, expand=expand
            )
            for code_string in code_strings
        ]
        _v_ = zip(line_ranges, code_strings, _v_)
        for (start_line, end_line), code_string, future in _v_:
            try:
                yield (start_line, end_line, code_string, future.result())

            except FStringError as exception:
                if on_f_string_error is None:
                    raise exception

                on_f_string_error(exception, start_line, code_string)
                continue


class _FileVisitor(libcst.CSTVisitor):
    METADATA_DEPENDENCIES = (l_metadata.PositionProvider,)

    def __init__(self, line_strings, line_ranges, temporary_name):
        super().__init__()

        self.line_strings = line_strings
        self.line_ranges = line_ranges
        self.temporary_name = temporary_name

        self._line_ranges = []

    def _visit_body(self, node):
        cst_node = node
        start_line = None

        for cst_node in cst_node.body:
            match cst_node:
                case libcst.SimpleStatementLine(
                    body=[
                        libcst.Assign(
                            targets=[
                                libcst.AssignTarget(target=libcst.Name(value=name))
                            ]
                        )
                    ]
                ) if name == self.temporary_name:
                    if start_line is None:
                        start_line = self._get_code_range(cst_node).start.line
                    continue

            if start_line is None:
                code_range = self._get_code_range(cst_node)

                end_line = self._get_end_line(cst_node, code_range=code_range)
                if end_line is None:
                    continue

                if 0 < (end_line - code_range.start.line):
                    self._add_line_range(code_range.start.line, end_line)

            else:
                self._add_line_range(start_line, self._get_end_line(cst_node))
                start_line = None

        if start_line is not None:
            self._add_line_range(start_line, self._get_end_line(cst_node))

    def _add_line_range(self, start_line, end_line):
        if self.line_ranges is not None and not any(
            range_start_line <= end_line and start_line <= range_end_line
            for range_start_line, range_end_line in self.line_ranges
        ):
            return

        self._line_ranges.append((start_line, end_line))

    visit_IndentedBlock = _visit_body
    visit_Module = _visit_body

    def _get_end_line(self, cst_node, code_range=None):
        _v_ = libcst.FunctionDef
        _v_ = (libcst.ClassDef, libcst.For, _v_, libcst.If, libcst.While, libcst.With)
        if isinstance(cst_node, _v_):
            if isinstance(cst_node.body, libcst.SimpleStatementSuite):
                return self._get_code_range(cst_node.body).end.line

            _v_ = self._get_code_range(cst_node.body.body[0]).start.line
            return self._get_before(_v_)

        if isinstance(cst_node, libcst.Match):
            return self._get_before(self._get_code_range(cst_node.cases[0]).start.line)

        if isinstance(cst_node, (libcst.Try, libcst.TryStar)):
            return

        _v_ = (self._get_code_range(cst_node) if code_range is None else code_range).end
        return _v_.line

    def _get_before(self, line_number):
        line_number -= 1
        while self.line_strings[line_number - 1] == "":
            line_number -= 1
        return line_number

    def _get_code_range(self, cst_node) -> l_metadata.CodeRange:
        _v_ = self.get_metadata(l_metadata.PositionProvider, cst_node)
        code_range = typing.cast(l_metadata.CodeRange, _v_)

        _v_ = isinstance(cst_node, (libcst.FunctionDef, libcst.ClassDef))
        if _v_ and len(cst_node.decorators) != 0:
            start_position = min(
                (
                    typing.cast(
                        l_metadata.CodeRange,
                        self.get_metadata(l_metadata.PositionProvider, decorator_node),
                    ).start
                    for decorator_node in cst_node.decorators
                ),
                key=lambda code_position: (code_position.line, code_position.column),
            )
            return l_metadata.CodeRange(start=start_position, end=code_range.end)

        return code_range


@contextlib.contextmanager
def _process_pool_executor(*args, **kwargs):
    pool_executor = c_futures.ProcessPoolExecutor(*args, **kwargs)
    try:
        yield pool_executor

    finally:
        pool_executor.shutdown(wait=True, cancel_futures=True)
