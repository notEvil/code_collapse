import code_collapse
import code_collapse.git as c_git
import black.mode as b_mode
import pydantic
import typer
import typing_extensions
import datetime
import enum
import logging
import pathlib
import sys
import time


class _LogLevel(enum.StrEnum):
    DEBUG = enum.auto()
    INFO = enum.auto()
    WARNING = enum.auto()
    ERROR = enum.auto()
    CRITICAL = enum.auto()


class _Configuration(pydantic.BaseModel):
    collapse_name: bool = False
    collapse_self: bool = False
    collapse_call_name: bool = False
    collapse_default: bool = True
    collapse_keyword_argument: bool = True
    collapse_assert: bool = False
    collapse_statement: bool = True
    collapse_literal: bool = False
    temporary_name: str = "_v_"
    _prefer_bottom: bool = False


def _validate_config(string):
    _Configuration.model_validate_json(string)
    return string


def main(
    paths: list[pathlib.Path],
    expand: bool = False,
    progress: typing_extensions.Annotated[
        bool, typer.Option(help="Show file path and `.` after each statement")
    ] = True,
    preview: bool = False,
    skip_magic_trailing_comma: typing_extensions.Annotated[
        bool, typer.Option("--skip-magic-trailing-comma", "-C")
    ] = False,
    config: typing_extensions.Annotated[
        str, typer.Option(parser=_validate_config, metavar="CONFIG")
    ] = _Configuration().model_dump_json(),
    git: typing_extensions.Annotated[
        bool, typer.Option(help="Skip unchanged statements")
    ] = False,
    git_index: typing_extensions.Annotated[
        bool, typer.Option(help="Use index instead of commit")
    ] = False,
    parallel: typing_extensions.Annotated[
        int, typer.Option(help="Use multiple processes")
    ] = 1,
    parallel_statements: typing_extensions.Annotated[
        bool, typer.Option(help="Not recommended. Process statements in parallel")
    ] = False,
    log_level: _LogLevel = _LogLevel.INFO,
):
    start_time = time.monotonic()

    logging.basicConfig(level=log_level.name)

    _v_ = not skip_magic_trailing_comma
    configuration = code_collapse.Configuration(
        black_mode=b_mode.Mode(magic_trailing_comma=_v_, preview=preview),
        **_Configuration.model_validate_json(config).model_dump(),
    )

    datetime_ = datetime.datetime.now()
    times = []

    if git:
        line_ranges = {
            path: {line_range for _, line_range in line_ranges}
            for repository_path in {c_git.get_repository_path(path) for path in paths}
            if repository_path is not None
            for path, line_ranges in c_git.get_line_ranges(
                c_git.get_diff_lines(repository_path, head=not git_index),
                repository_path,
            )
        }

    else:
        line_ranges = None

    def _main(paths):
        start_time = 0

        for path in paths:
            if path.is_dir():
                _main(path.iterdir())

            elif path.is_file() and path.suffix.lower() == ".py":
                if line_ranges is not None:
                    file_line_ranges = line_ranges.get(path.resolve())
                    if file_line_ranges is None:
                        continue

                else:
                    file_line_ranges = None

                def on_f_string_error(_, start_line, code_string):
                    if progress:
                        print(file=sys.stderr)

                    _v_ = "\n|".join(code_string.splitlines())
                    _v_ = f"code_collapse.FStringError at {path}:{start_line}\n|{_v_}"
                    logging.info(_v_)

                if progress:
                    print(path, end=" ", file=sys.stderr)
                    sys.stderr.flush()

                    start_time = time.monotonic()

                for line_string in code_collapse.collapse_file(
                    path,
                    expand=expand,
                    configuration=configuration,
                    line_ranges=file_line_ranges,
                    on_f_string_error=on_f_string_error,
                    progress=progress,
                    datetime_=datetime_,
                    executor=None if parallel == 1 else pool_executor,
                    statements=parallel_statements,
                ):
                    print(line_string)

                if progress:
                    seconds = time.monotonic() - start_time
                    times.append(seconds)

                    print(f" {_get_string(seconds)}", file=sys.stderr)

    with code_collapse._process_pool_executor(max_workers=parallel) as pool_executor:
        _main(paths)

    if progress:
        seconds = time.monotonic() - start_time

        print(file=sys.stderr)
        if len(times) == 0:
            print(" 0 files", file=sys.stderr)

        else:
            print(
                f" {len(times)} files | {_get_string(seconds)} | avg:"
                f" {_get_string(sum(times) / len(times))} | max:"
                f" {_get_string(max(times))}",
                file=sys.stderr,
            )


def _get_string(seconds):
    return f"{int(seconds * 1e3)} ms" if seconds < 1 else f"{seconds:0.1f} s"


typer.run(main)
