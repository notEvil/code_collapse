import pynvim
import collections.abc as c_abc
import contextlib
import os
import pathlib
import re
import subprocess
import sys
import tempfile
import time
import typing


PATH = pathlib.Path(__file__).parent


path_string = str(PATH.parent.parent / "src")
if path_string not in sys.path:
    sys.path.append(path_string)


@pynvim.plugin
class CodeCollapse:
    def __init__(self, nvim):
        super().__init__()

        self.nvim = nvim

        self._stopped = False

    @pynvim.command(
        "CollapseCode",
        range=True,
        sync=False,
        nargs="*",  # pyright: ignore [reportGeneralTypeIssues]
    )  # fmt: skip
    def collapse_code(self, args, range):  # col: skip
        buffer_id = self.nvim.api.win_get_buf(0)
        with self._modifiable(False, buffer_id):
            try:
                arguments = dict(self._parse_args(args))
                expand = bool(int(arguments.get("expand", "0")))

                expand_string = "Expand" if expand else "Collaps"
                self.nvim.out_write(f"{expand_string}ing code ..\n")

                self._prepare()

                import code_collapse

                start_time = time.monotonic()

                start_line, end_line = range
                line_strings = self.nvim.api.buf_get_lines(
                    buffer_id, start_line - 1, end_line, True
                )
                code_string = "\n".join(line_strings)
                configuration = self._get_configuration()

                try:
                    code_string = code_collapse.collapse_code(
                        code_string, expand=expand, configuration=configuration
                    )

                except code_collapse.FStringError:
                    raise _Exception("FStringError")

                collapsed_lines = typing.cast(str, code_string).splitlines()
                if collapsed_lines != line_strings:
                    with self._modifiable(True, buffer_id):
                        self.nvim.api.buf_set_lines(
                            buffer_id, start_line - 1, end_line, True, collapsed_lines
                        )

                _v_ = self._get_time_string(time.monotonic() - start_time)
                self.nvim.out_write(f"{expand_string}ed code in {_v_}\n")

            except _Exception as exception:
                (message_string,) = exception.args
                self.nvim.err_write(f"{message_string}\n")

    @pynvim.command(
        "CollapseFile",
        sync=False,
        nargs="*",  # pyright: ignore [reportGeneralTypeIssues]
    )  # fmt: skip
    def collapse_file(self, args):  # col: skip
        buffer_id = self.nvim.api.win_get_buf(0)
        with self._modifiable(False, buffer_id):
            self._stopped = False

            try:
                arguments = dict(self._parse_args(args))
                expand = bool(int(arguments.get("expand", "0")))
                git = bool(int(arguments.get("git", "0")))
                git_index = bool(int(arguments.get("git_index", "0")))
                parallel = int(arguments.get("parallel", "1"))

                expand_string = "Expand" if expand else "Collaps"
                self.nvim.out_write(f"{expand_string}ing file ..\n")

                self._prepare()

                import code_collapse

                start_time = time.monotonic()
                line_count = self.nvim.api.buf_line_count(buffer_id)

                _v_ = self.nvim.api.buf_get_lines(buffer_id, 0, line_count, False)
                line_strings = _v_

                code_string = "\n".join(line_strings)

                if git:
                    line_ranges = self._get_line_ranges(
                        line_strings, code_string, not git_index, buffer_id
                    )

                    if line_ranges is not None and len(line_ranges) == 0:
                        self.nvim.out_write("Nothing to do\n")
                        return

                else:
                    line_ranges = None

                offset = 0
                index = -1

                if not self._stopped:
                    _v_ = code_collapse._process_pool_executor(max_workers=parallel)
                    with _v_ as pool_executor:
                        _v_ = code_collapse._collapse_file(
                            code_string,
                            line_strings,
                            expand,
                            self._get_configuration(),
                            line_ranges,
                            self._on_f_string_error,
                            None if parallel == 1 else pool_executor,
                            False,
                        )
                        for index, (
                            start_line,
                            end_line,
                            code_string,
                            collapsed_string,
                        ) in enumerate(_v_):
                            if collapsed_string != code_string:
                                line_strings = collapsed_string.splitlines()

                                with self._modifiable(True, buffer_id):
                                    self.nvim.api.buf_set_lines(
                                        buffer_id,
                                        start_line - 1 + offset,
                                        end_line + offset,
                                        True,
                                        line_strings,
                                    )

                                _v_ = len(line_strings) - (end_line - start_line + 1)
                                offset += _v_

                            if self._stopped:
                                break

                            _v_ = [
                                f"{expand_string}ed {index + 1} statements .."
                                f" {end_line / line_count * 100:.1f} %"
                            ]
                            self.nvim.api.echo([_v_], False, {})

                _v_ = self._get_time_string(time.monotonic() - start_time)
                _v_ = f"{expand_string}ed {index + 1} statements in {_v_}\n"
                self.nvim.out_write(_v_)

            except _Exception as exception:
                (message_string,) = exception.args
                self.nvim.err_write(f"{message_string}\n")

    def _get_line_ranges(self, line_strings, code_string, head, buffer_id):
        import code_collapse.git as c_git

        path_string = self.nvim.api.buf_get_name(buffer_id)
        if path_string is None:
            return

        file_path = pathlib.Path(path_string)
        repository_path = c_git.get_repository_path(file_path)
        if repository_path is None:
            return

        # not modified; use code_collapse.git
        if not self.nvim.api.buf_get_option(buffer_id, "mod"):
            _v_ = ["git", "ls-files", "--error-unmatch", str(file_path)]
            return_code = subprocess.Popen(_v_, cwd=repository_path).wait()

            if not (return_code in [0, 1]):
                raise _Exception(f"git ls-files failed with return code {return_code}")

            if return_code == 1:
                return

            try:
                _v_ = c_git.get_diff_lines(repository_path, path=file_path, head=head)
                return {
                    line_range
                    for _, line_ranges in c_git.get_line_ranges(_v_, repository_path)
                    for _, line_range in line_ranges
                }

            except c_git.ReturnCodeError as exception:
                _v_ = f"git diff failed with return code {exception.return_code}"
                raise _Exception(_v_)
        #

        def _get_git_file(file=None):
            read = file is None
            commit_string = "HEAD" if head else ""

            _v_ = f"{commit_string}:{file_path.relative_to(repository_path)}"
            _v_ = ["git", "show", _v_]
            process = subprocess.Popen(
                _v_, stdout=subprocess.PIPE if read else file, cwd=repository_path
            )

            code_string = (
                typing.cast(typing.IO, process.stdout).read().decode() if read else None
            )

            return_code = process.wait()
            if not (return_code in [0, 128]):
                raise _Exception(f"git show failed with return code {return_code}")

            return code_string if read else None

        # "inexpensive"; use vim.diff
        if len(line_strings) <= 5000:
            _v_ = dict(result_type="indices", ctxlen=0, interhunkctxlen=0)
            _v_ = self.nvim.lua.vim.diff(_get_git_file(), code_string, _v_)
            return {(l2, l2 + s2) for _, _, l2, s2 in _v_}
        #

        with tempfile.TemporaryDirectory() as temp_path_string:
            temp_path = pathlib.Path(temp_path_string)

            with open(temp_path / "a", "wb") as file:
                _get_git_file(file=file)

            with open(temp_path / "b", "w") as file:
                file.write(code_string)

            _v_ = ["diff", "--unified=0", str(temp_path / "a"), str(temp_path / "b")]
            process = subprocess.Popen(_v_, stdout=subprocess.PIPE)

            _v_ = typing.cast(typing.IO, process.stdout)
            _v_ = (line_bytes.decode() for line_bytes in _v_)
            _v_ = c_git.get_line_ranges(_v_, repository_path)
            line_ranges = {
                line_range for _, line_ranges in _v_ for _, line_range in line_ranges
            }

            return_code = process.wait()
            if not (return_code in [0, 1]):
                raise _Exception(f"diff failed with return code {return_code}")

        return line_ranges

    def _parse_args(self, strings) -> c_abc.Generator[tuple[str, str], None, None]:
        for string in strings:
            match = re.search(r"^(?P<name>\w+)=(?P<string>.*)", string)
            if match is None:
                raise _Exception("Invalid arguments")

            yield typing.cast(tuple[str, str], match.group("name", "string"))

    def _on_f_string_error(self, exception, start_line, code_string):
        self.nvim.err_write(f"FStringError at line {start_line}\n")

    def _prepare(self):
        # required to parse Python 3.10 and 3.11 grammar with libcst <1.0.0
        if os.environ.get("LIBCST_PARSER_TYPE") is None:
            os.environ["LIBCST_PARSER_TYPE"] = "native"

    def _get_configuration(self):
        import code_collapse
        import black.mode as b_mode

        _v_ = b_mode.Mode(magic_trailing_comma=False)
        return code_collapse.Configuration(black_mode=_v_, collapse_assert=True)

    @contextlib.contextmanager
    def _modifiable(self, bool, buffer_id):
        if bool == self.nvim.api.buf_get_option(buffer_id, "modifiable"):
            yield
            return

        self.nvim.api.buf_set_option(buffer_id, "modifiable", bool)
        try:
            yield

        finally:
            self.nvim.api.buf_set_option(buffer_id, "modifiable", not bool)

    def _get_time_string(self, seconds):
        return f"{round(seconds * 1_000)} ms" if seconds < 1 else f"{seconds:.1f} s"

    @pynvim.command("CollapseFileStop")
    def stop(self):
        self._stopped = True


class _Exception(Exception):
    pass
