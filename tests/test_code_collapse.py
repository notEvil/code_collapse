import code_collapse
import black


assert black.Mode().line_length == 88


def _get_configuration(collapse_name=True, collapse_literal=True, **kwargs):
    return code_collapse.Configuration(
        collapse_name=collapse_name, collapse_literal=collapse_literal, **kwargs
    )


def s(length):
    string = f"{length}".ljust(length - 2)
    return f'"{string}"'


def v(length):
    return f"_{length}".ljust(length, "_")[:length]


def i(length):
    zero_string = "0" * length
    return f"{length}{zero_string[len(str(length)):]}"


def f(length):
    return f"{i(length)[:-2]}.0"


def c(length):
    return f"{i(length)[:-5]} + 0j"


def test_trivial():
    _test(
        """
pass
""",
        """
pass
""",
    )


def test_indentation():
    _test(
        """
    pass
""",
        """
    pass
""",
    )


def test_partial_statements():
    _test(
        """
if False:
""",
        """
if False:
""",
    )


def test_top_nodes():
    _test(
        """
False
True
""",
        """
False
True
""",
    )


def test_last_node():
    _test(
        """
_ = 0
_ = _ + 1
_ = _ + 2
""",
        """
_ = 0 + 1 + 2
""",
    )


def test_untouched():
    _test(
        f"""
({s(50)}, {s(51)}, _)
""",
        f"""
({s(50)}, {s(51)}, _)
""",
    )

    _test(
        f"""
import {v(82)}
""",
        f"""
import {v(82)}
""",
    )

    _test(
        f"""
from {v(50)} import {v(51)}
""",
        f"""
from {v(50)} import (
    {v(51)},
)
""",
    )

    _test(
        f"""
    global {v(78)}
""",
        f"""
    global {v(78)}
""",
    )

    _test(
        f"""
    nonlocal {v(76)}
""",
        f"""
    nonlocal {v(76)}
""",
    )


def test_trivial_statements():
    _test(
        f"""
name = {s(82)}
""",
        f"""
_ = {s(82)}
name = _
""",
    )


def test_conditionals():
    _test(  # if expression
        f"""
{s(50)} if False else {s(51)}
""",
        f"""
{s(50)} if False else {s(51)}
""",
    )

    _test(  # boolean operation
        f"""
False and {s(80)}
""",
        f"""
False and {s(80)}
""",
    )


def test_concatenated_strings():
    _test(
        f"""
function({s(39)} {s(39)})
""",
        f"""
_ = {s(39)} {s(39)}
function(_)
""",
    )

    _test(
        f"""
(f"{{{v(50)}}}" {s(51)})
""",
        f"""
_ = {v(50)}
(f"{{_}}" {s(51)})
""",
    )


def test_del():
    _test(
        f"""
del {v(50)}, {v(51)}
""",
        f"""
del (
    {v(50)},
    {v(51)},
)
""",
    )

    _test(
        f"""
del {v(50)}.{v(51)}
""",
        f"""
_ = {v(50)}
del _.{v(51)}
""",
    )

    _test(
        f"""
del {v(50)}[{v(51)}]
""",
        f"""
_ = {v(51)}
del {v(50)}[_]
""",
    )


def test_with():
    _test(
        f"""
with a({s(50)}), b({s(51)}):
    pass
""",
        f"""
_ = a({s(50)})
with _, b({s(51)}):
    pass
""",
    )


def test_collapse_name():
    _test(
        f"""
function({v(80)})
""",
        f"""
function(
    {v(80)}
)
""",
        configuration=_get_configuration(collapse_name=False),
    )

    _test(
        f"""
function({v(80)})
""",
        f"""
_ = {v(80)}
function(_)
""",
        configuration=_get_configuration(collapse_name=True),
    )


def test_collapse_call_name():
    _test(
        f"""
{v(52)}({v(51)})
""",
        f"""
_ = {v(51)}
{v(52)}(_)
""",
        configuration=_get_configuration(collapse_call_name=False),
    )

    _test(
        f"""
{v(52)}({v(51)})
""",
        f"""
_ = {v(52)}
_({v(51)})
""",
        configuration=_get_configuration(collapse_call_name=True),
    )

    _test(
        f"""
object.{v(52)}({v(51)})
""",
        f"""
_ = {v(51)}
object.{v(52)}(_)
""",
        configuration=_get_configuration(collapse_call_name=False),
    )

    _test(
        f"""
object.{v(52)}({v(51)})
""",
        f"""
_ = object.{v(52)}
_({v(51)})
""",
        configuration=_get_configuration(collapse_call_name=True),
    )


def test_collapse_self():
    _test(
        f"""
function(self.{v(74)})
""",
        f"""
function(
    self.{v(74)}
)
""",
        configuration=_get_configuration(collapse_self=False),
    )

    _test(
        f"""
function(self.{v(74)})
""",
        f"""
_ = self.{v(74)}
function(_)
""",
        configuration=_get_configuration(collapse_self=True),
    )


def test_collapse_default():
    _test(
        f"""
def function(name={s(80)}):
""",
        f"""
def function(
    name={s(80)},
):
""",
        configuration=_get_configuration(collapse_default=False),
    )

    _test(
        f"""
def function(name={s(80)}):
""",
        f"""
_ = {s(80)}


def function(name=_):
""",
        configuration=_get_configuration(collapse_default=True),
    )


def test_collapse_keyword_argument():
    _test(
        f"""
function(name={s(80)})
""",
        f"""
function(
    name={s(80)}
)
""",
        configuration=_get_configuration(collapse_keyword_argument=False),
    )

    _test(
        f"""
function(name={s(80)})
""",
        f"""
_ = {s(80)}
function(name=_)
""",
        configuration=_get_configuration(collapse_keyword_argument=True),
    )


def test_collapse_assert():
    _test(
        f"""
assert {s(82)}
""",
        f"""
assert (
    {s(82)}
)
""",
        configuration=_get_configuration(collapse_assert=False),
    )

    _test(
        f"""
assert {s(82)}
""",
        f"""
_ = {s(82)}
assert _
""",
        configuration=_get_configuration(collapse_assert=True),
    )


def test_collapse_statement():
    _test(
        f"""
name = {s(82)}
""",
        f"""
name = (
    {s(82)}
)
""",
        configuration=_get_configuration(collapse_statement=False),
    )

    _test(
        f"""
name = {s(82)}
""",
        f"""
_ = {s(82)}
name = _
""",
        configuration=_get_configuration(collapse_statement=True),
    )


def test_expand_yield():
    _test(
        """
_ = yield
_
""",
        """
_ = yield
_
""",
    )

    _test(
        """
_ = yield from []
_
""",
        """
_ = yield from []
_
""",
    )

    _test(
        """
top_node
_ = None
_ = yield _
_ = function(_)
_
""",
        """
top_node
_ = yield None
function(_)
""",
    )


def test_expand_comment():
    _test(
        """
_ = None  # comment
_
""",
        """
_ = None  # comment
_
""",
    )

    _test(
        f"""
_ = (
    {v(50)},
    {v(51)},  # comment
)
_
""",
        f"""
_ = (
    {v(50)},
    {v(51)},  # comment
)
_
""",
        configuration=_get_configuration(collapse_name=False),
    )


def test_expand_fstring():
    _test(
        f"""
_ = (
    {v(50)},
    {v(51)},
)
f"{{_}}"
""",
        f"""
f"{{( {v(50)}, {v(51)}, )}}"
""",
    )  # Warning: assumes Black doesn't touch expressions inside f-strings


def test_parenthesize_comp_for():
    _test(
        """
_ = [True] if True else [False]
(bool for bool in _)
""",
        """
(bool for bool in ([True] if True else [False]))
""",
    )


def test_collapse_literal():
    for generator in (i, f, c, s):
        _test(
            f"""
name = {generator(82)}
""",
            f"""
name = (
    {generator(82)}
)
""",
            configuration=_get_configuration(collapse_literal=False),
        )

        _test(
            f"""
name = {generator(82)}
""",
            f"""
_ = {generator(82)}
name = _
""",
            configuration=_get_configuration(collapse_literal=True),
        )


def test_collapse_typing_cast():
    _test(
        f"""
typing.cast({v(51)}, {v(50)})
""",
        f"""
_ = {v(50)}
typing.cast({v(51)}, _)
""",
    )


def test_while():
    _test(
        f"""
while ({v(50)}, {v(51)}):
    pass
""",
        f"""
while (
    {v(50)},
    {v(51)},
):
    pass
""",
    )


def test_typing_TypeVar():
    for annotation_string in ["", ": typing.TypeVar"]:
        for function_string in ["typing.", ""]:
            _test(
                f"""
name{annotation_string} = {function_string}TypeVar({v(50)}, {v(51)})
""",
                f"""
name{annotation_string} = {function_string}TypeVar(
    {v(50)},
    {v(51)},
)
""",
            )


def test_typing_TypeAlias():
    for annotation_string in ["", "typing."]:
        _test(
            f"""
name: {annotation_string}TypeAlias = ({v(50)}, {v(51)})
""",
            f"""
name: {annotation_string}TypeAlias = (
    {v(50)},
    {v(51)},
)
""",
        )


def _test(code_string, expected_code, configuration=None):
    if configuration is None:
        configuration = _get_configuration()

    configuration.temporary_name = "_"

    code_string = code_string[1:-1]
    expected_code = expected_code[1:-1]

    assert (
        code_collapse.collapse_code(code_string, configuration=configuration)
        == expected_code
    )
