# code_collapse

is a Python package providing a method for increasing the code density of Python statements which do not fit on a single line. It uses [*LibCST*](https://github.com/Instagram/LibCST) for code parsing, analysis and transformations, and [*Black*](https://github.com/psf/black) for code formatting.

**Examples**

from [https://black.readthedocs.io/en/stable/the_black_code_style/current_style.html](https://black.readthedocs.io/en/stable/the_black_code_style/current_style.html)
```python
# in:
    result = (
        session.query(models.Customer.id)
        .filter(
            models.Customer.account_id == account_id,
            models.Customer.email == email_address,
        )
        .order_by(models.Customer.id.asc())
        .all()
    )
# out:
    _v_ = session.query(models.Customer.id).filter(
        models.Customer.account_id == account_id, models.Customer.email == email_address
    )
    result = _v_.order_by(models.Customer.id.asc()).all()
```
from *code_collapse*
```python
# in:
            self.cst_nodes.append(
                libcst.SimpleStatementLine(
                    body=[_get_assign(_TEMPORARY_NODE, _wrap_node(reduce_node))]
                )
            )
# out:
            _v_ = [_get_assign(_TEMPORARY_NODE, _wrap_node(reduce_node))]
            self.cst_nodes.append(libcst.SimpleStatementLine(body=_v_))
```

## Features

- Indentation and partial statements
    - e.g. `    return {expression}`, `if {expression}:`
- Targets the last statement and keeps the rest untouched
- Idempotent / stable
- Readable by default
    - e.g. doesn't separate names from call parentheses
    - see section [Details](#details) for more
- Correct
    - e.g. doesn't touch conditional expressions like `...` in `... if {expression} else ...`, `{expression} and ...`
- Process files/directories and create a patch
- Ignore unchanged statements in Git repositories
- Neovim integration
- Parallel
- "Exhaustive"
    - considers "all" possible transformations and chooses the best
    - see section [Details](#details)

## Quickstart

- Install [*Pipenv*](https://github.com/pypa/pipenv)
    - e.g. `python -m pip install pipenv`
- Install *code_collapse*
    - e.g. `python -m pipenv run pip install -e .[all]`
- Run tests
    - e.g. `python -m pipenv run python -m pytest .`
- or use the CLI
    - e.g. `python -m pipenv run python -m code_collapse --help`
- or import *code_collapse* and call `code_collapse.collapse_code` or `code_collapse.collapse_file`

### Neovim

- Install *code_collapse* for the Python environment used by Neovim
- Install *code_collapse* as Neovim plugin using your favorite plugin manager
    - e.g. `require('lazy').setup({ {'notEvil/code_collapse', url='https://gitlab.com/notEvil/code_collapse'} })`
- Start Neovim and type `:UpdateRemotePlugins<cr>`
- Restart Neovim
- Type `:CollapseCode<cr>` in Visual mode, or `:CollapseFile<cr>` in Normal mode
    - optionally add `expand=1`, `git=1`, `git_index=1`, `parallel={n}`
- Type `:CollapseFileStop<cr>` in Normal mode to stop collapsing

## Motivation

If you don't know *Black*, check it out first!

While programming I never assign intermediate results to variables because it makes assignments and variables more informative. This leads to heavily nested expressions that won't fit on a single line. *Black* introduces lots of whitespace in this case as can be seen in the following example:

```python
def function(argument):
    pass

# 8:
function(function(function(function(function(function(function(function())))))))
# 9:
function(
    function(function(function(function(function(function(function(function())))))))
)
# 10:
function(
    function(
        function(function(function(function(function(function(function(function())))))))
    )
)
```

(Keep in mind that actual expressions are way more diverse and don't need 8 levels to reach this point.) The solution: find a suitable sub expression and assign it to a temporary variable like `_` on the line above.

```python
# 9:
_ = function(function(function(function(function(function(function(function())))))))
function(_)
# 10:
_ = function(function(function(function(function(function(function(function())))))))
function(function(_))
```

When adding levels on the outside, this is easy. Just insert `_ = ` as soon as the expression doesn't fit on the line anymore and continue on a new line below. However, when changing those expressions, they quickly become sparse and rewriting them to dense equivalents rather difficult.

## Details

- Default configuration
    - `black_mode = black.Mode(magic_trailing_comma=False)`
    - `collapse_name = False`
        - no `_v_ = {name}`
    - `collapse_self = False`
        - no `_v_ = self.{name}`
    - `collapse_call_name = False`
        - no `_v_ = {expression}.{name}` > `_v_(...)`
    - `collapse_default = True`
        - e.g. `def function(keyword=_v_)`
    - `collapse_keyword_argument = True`
        - e.g. `function(keyword=_v_)`
    - `collapse_assert = False`
        - keep asserts untouched
    - `collapse_statement = True`
        - e.g. `{name} = _v_`, `yield _v_`
    - `collapse_literal = False`
        - no `_v_ = {int or float or complex or single-line string}`
    - `temporary_name = "_v_"`
- If the statement to collapse depends on `_v_`, the code is returned untouched
- If a temporary statement (`_v_ = {expression}`) has a comment, it is treated like the last statement

**"Exhaustive"?**

The algorithm is exhaustive only on a subset of possible transformations. It doesn't consider expressions where the parent expression fits on the same number of lines. This is a "greedy" approach which might not yield truly optimal results. However, the result should be more stable in case of small changes, and the user might expect the algorithm to be "greedy" in the first place and consider the result good enough.

An alternative approach would be to stop considering sub transformations when they can't possibly yield a better result than the currently best. It would yield truly optimal results, but I don't know if its worth.

## Troubleshooting

### `code_collapse.collapse_code` raises `code_collapse.FStringError`

Currently, the use of strings inside f-strings is limited and supporting this would be non-trivial. See [PEP 701](https://peps.python.org/pep-0701/).

#### `ParserSyntaxError: ... Incomplete input. Encountered end of file (EOF), but expect 'INDENT'.`

You should upgrade to a more recent version of *LibCST*. If you can't, you can try to set the environment variable `LIBCST_PARSER_TYPE=native`.
